unit uCadBairro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.ToolWin, cxGraphics, Geral,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, dxBarBuiltInMenu, cxContainer, cxEdit,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB,
  cxDBData, cxCheckBox, cxDBEdit, cxDBLookupComboBox, Vcl.StdCtrls, cxPC,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxTextEdit, cxMaskEdit, Vcl.ExtCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Actions, Vcl.ActnList,
  Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan, ppEndUsr, ppProd, ppClass,
  ppReport, ppComm, ppRelatv, ppDB, ppDBPipe, ppCtrls, ppPrnabl, ppBands,
  ppCache, ppDesignLayer, ppParameter, System.ImageList, Vcl.ImgList,
  cxImageList;

type
  TfCadBairro = class(TForm)
    ToolBar1: TToolBar;
    btnIncluir: TToolButton;
    pagDados: TcxPageControl;
    tabGrid: TcxTabSheet;
    Panel8: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    tabDados: TcxTabSheet;
    cxTabControl2: TcxTabControl;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    edtDescricao: TcxDBTextEdit;
    Panel6: TPanel;
    btnEditar: TToolButton;
    btnGravar: TToolButton;
    btnCancelar: TToolButton;
    btnExcluir: TToolButton;
    qryGrid: TFDQuery;
    DSGrid: TDataSource;
    qrySelect: TFDQuery;
    cxDBTextEdit1: TcxDBTextEdit;
    qryDados: TFDQuery;
    DSDados: TDataSource;
    ToolButton9: TToolButton;
    ToolButton1: TToolButton;
    ToolButton7: TToolButton;
    ACBotoes: TActionManager;
    acIncluir: TAction;
    acEditar: TAction;
    acGravar: TAction;
    acCancelar: TAction;
    acExcluir: TAction;
    acRelatorio: TAction;
    acSair: TAction;
    ToolButton2: TToolButton;
    ppDBPipeline1: TppDBPipeline;
    ppDesigner1: TppDesigner;
    ppDesignLayer1: TppDesignLayer;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    qryGridCD_BAIRRO: TIntegerField;
    qryGridDS_BAIRRO: TStringField;
    qryGridDT_INCALT: TSQLTimeStampField;
    qryGridCD_USRINCALT: TStringField;
    qryGridDS_BAIRR2: TStringField;
    cxGrid1DBTableView1CD_BAIRRO: TcxGridDBColumn;
    cxGrid1DBTableView1DS_BAIRRO: TcxGridDBColumn;
    qryDadosCD_BAIRRO: TIntegerField;
    qryDadosDS_BAIRRO: TStringField;
    qryDadosDT_INCALT: TSQLTimeStampField;
    qryDadosCD_USRINCALT: TStringField;
    qryDadosDS_BAIRR2: TStringField;
    StaticText1: TStaticText;
    cxTextEdit1: TcxTextEdit;
    StaticText2: TStaticText;
    cxTextEdit2: TcxTextEdit;
    ImagemBotao32: TcxImageList;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure AbreQuery;
    procedure ToolButton1Click(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure acSairExecute(Sender: TObject);
    procedure acGravarExecute(Sender: TObject);
    procedure acRelatorioExecute(Sender: TObject);
    procedure acExcluirExecute(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
    procedure acIncluirExecute(Sender: TObject);
    procedure acCancelarExecute(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure acEditarExecute(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cbxUFKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    bEditaRel : Boolean;
    function ValidarCampos : Boolean;
    procedure AtualizaGrid;
    procedure ControlaBotao;
  public
    { Public declarations }
  end;

var
  fCadBairro: TfCadBairro;

implementation

{$R *.dfm}

uses untDMBasicos, uPrincipal, uCadCidade;

procedure TfCadBairro.AbreQuery;
begin
  qryGrid.Close;
  qryGrid.Open();
end;

procedure TfCadBairro.acCancelarExecute(Sender: TObject);
begin
  if qryDados.State in [dsInsert, dsEdit] then
  begin
    qryDados.Cancel;
    pagDados.ActivePage := tabGrid;
    ControlaBotao;
  end;
end;

procedure TfCadBairro.acEditarExecute(Sender: TObject);
begin
  if not qryGrid.IsEmpty then
  begin
    try
      qryDados.Close;
      qryDados.ParamByName('cd_bairro').AsInteger := qryGrid.FieldByName('cd_bairro').AsInteger;
      qryDados.Open;
      qryDados.Edit;

      pagDados.ActivePage := tabDados;
      SetaFoco(edtDescricao);
      ControlaBotao;
    except
      On E:Exception do
      begin
        pagDados.ActivePage := tabGrid;
        MensagemErro('Erro ao alterar o registro!'+sLineBreak+ 'Erro: '+E.Message);
      end;
    end;
  end;
end;

procedure TfCadBairro.acExcluirExecute(Sender: TObject);
begin
  if MensagemConfirmacao('Deseja excluir o registro selecionado ?') = IDNO then
    Abort
  else
  begin
    try
    //  StartTransaction(DmBasicos.FDConnection);

      if (qryGrid.RecordCount > 0 ) then
        qryGrid.Delete;

    //  CommitTransaction(DmBasicos.FDConnection);


      ControlaBotao;
    except
      On E:Exception do
      begin
        pagDados.ActivePage := tabGrid;
        RollbackTransaction(DmBasicos.FDConnection);
        MensagemErro('Erro ao excluir o registro!' + sLineBreak + 'Erro: ' + E.Message);
      end;
    end;
  end;
end;

procedure TfCadBairro.acGravarExecute(Sender: TObject);
begin
  if not ValidarCampos then
    Exit;

  if not (qryDados.State in [dsEdit, dsInsert]) then
    Exit;

  try
   // StartTransaction(DmBasicos.FDConnection);


    if qryDados.State = dsInsert then
    begin
      qrySelect.Close;
      qrySelect.SQL.Clear;
      qrySelect.SQL.Add('Select MAX(CD_BAIRRO) AS QT ' );
      qrySelect.SQL.Add('  From BAIRRO ' );
      qrySelect.Open;

      qryDados.FieldByName('CD_BAIRRO').AsInteger :=  qrySelect.FieldByName('QT').AsInteger + 1;
    end;

    qryDados.Post;
    pagDados.ActivePage := tabGrid;

    //CommitTransaction(DmBasicos.FDConnection);

    AtualizaGrid;

    SetaFoco(cxGrid1);
    ControlaBotao;
  except on E: Exception do
    begin
   //   DmBasicos.FDConnection.Rollback;
      pagDados.ActivePage := tabGrid;
      MensagemErro('Erro ao gravar registro no banco de dados...' + sLineBreak + 'Erro: ' + E.Message);
    end;
  end;
end;

procedure TfCadBairro.acIncluirExecute(Sender: TObject);
begin
  try
    qryDados.Close;
    qryDados.ParamByName('cd_bairro').AsInteger := -1;
    qryDados.Open;
    qryDados.Append;

    pagDados.ActivePage := tabDados;

    SetaFoco(edtDescricao);
    ControlaBotao;
  except on E: Exception do
    begin
      pagDados.ActivePage := tabGrid;
      MensagemErro('Erro ao incluir um registro...' + sLineBreak + 'Erro: ' + E.Message);
    end;
  end;
end;

procedure TfCadBairro.acRelatorioExecute(Sender: TObject);
begin
  try
    if not qryGrid.IsEmpty then
    begin
      if bEditaRel then
      begin
        //

      end;

      ppReport1.Print;


      // relatorio tal imprimie
    end;
	except
		on E: Exception do begin
      pagDados.ActivePage := tabGrid;
		  MensagemErro('Erro ao imprimir relat�rio...' + sLineBreak + 'Erro: ' + e.Message);
		  exit;
		end;
	end;
end;

procedure TfCadBairro.acSairExecute(Sender: TObject);
begin
  Close;
end;

procedure TfCadBairro.AtualizaGrid;
begin
  qryGrid.Close;
  qryGrid.Open;
end;

procedure TfCadBairro.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  acEditar.Execute;
end;

procedure TfCadBairro.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if qryDados.State in [dsInsert, dsEdit] then
    qryDados.Cancel;

  fCadBairro := nil;
  Action := CaFree;
end;

procedure TfCadBairro.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if      (Key = vk_insert ) and (DSDados.DataSet.State = dsBrowse) then
          acIncluir.Execute
  else if (Key = 27)        and (DSDados.DataSet.State = dsBrowse) then
          Close
  else if (Key = 27)        and (DSDados.DataSet.State <> dsBrowse) then
          begin
            acCancelar.Execute;

            key := 0 ;
          end
  else if (Key = vk_delete) and (DSDados.DataSet.State = dsBrowse) then
          acExcluir.Execute
  else if (shift=[ssCtrl]) and (Key = vk_F7) then
          begin
            if not bEditaRel then
            begin
              bEditaRel := true;
              MensagemInformacao('Op��o editar relat�rio ATIVADO');
            end
            else
            begin
              bEditaRel := false;
              MensagemInformacao('Op��o editar relat�rio DESATIVADO');
            end;
          end;
end;

procedure TfCadBairro.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if      (Key = #27) and (DSDados.DataSet.State = dsBrowse) then
          Close
  else if Key = #13 then
          Perform(wm_nextdlgctl, 0, 0);
end;

procedure TfCadBairro.FormShow(Sender: TObject);
begin
  pagDados.ActivePage := tabGrid;
  pagDados.HideTabs   := True;

  AbreQuery;
  ControlaBotao;
end;

procedure TfCadBairro.ToolButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfCadBairro.btnIncluirClick(Sender: TObject);
begin
  acIncluir.Execute;
end;

procedure TfCadBairro.cbxUFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
    btnGravar.Click;
end;

procedure TfCadBairro.ControlaBotao;
begin
  if ((DSGrid.State in [dsBrowse, dsInactive]) and (DSGrid.DataSet.RecordCount = 0)) and
      (DSDados.State in [dsBrowse, dsInactive])                                      then  // em branco, sem registro
  begin
    btnIncluir.Enabled    := true;
    btnEditar.Enabled     := false;
    btnGravar.Enabled     := false;
    btnCancelar.Enabled   := false;
    btnExcluir.Enabled    := false;
  end
  else if (DSGrid.DataSet.RecordCount > 0) and (DSDados.State in [dsBrowse, dsInactive]) then
  begin
    btnIncluir.Enabled    := true;
    btnEditar.Enabled     := true;
    btnGravar.Enabled     := false;
    btnCancelar.Enabled   := false;
    btnExcluir.Enabled    := true;
  end
  else if DSDados.State in [dsInsert, dsEdit] then
  begin
    btnIncluir.Enabled    := false;
    btnEditar.Enabled     := false;
    btnGravar.Enabled     := true;
    btnCancelar.Enabled   := true;
    btnExcluir.Enabled    := false;
  end;
end;

procedure TfCadBairro.btnCancelarClick(Sender: TObject);
begin
  acCancelar.Execute;
end;

procedure TfCadBairro.ToolButton6Click(Sender: TObject);
begin
  acExcluir.Execute;
end;

function TfCadBairro.ValidarCampos: Boolean;
begin
  Result := False;

  if Trim(edtDescricao.Text) = EmptyStr then
  begin
    MensagemAtencao('Falta informar a descri��o!');
    edtDescricao.SetFocus;
    Abort;
  end;


  Result := true;
end;

end.

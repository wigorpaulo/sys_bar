object fCadGer: TfCadGer
  Left = 0
  Top = 0
  Caption = 'CadGer'
  ClientHeight = 511
  ClientWidth = 872
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIForm
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 867
    Top = 5
    Width = 5
    Height = 501
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 0
  end
  object Panel2: TPanel
    Left = 0
    Top = 5
    Width = 5
    Height = 501
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 872
    Height = 5
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
  end
  object Panel4: TPanel
    Left = 0
    Top = 506
    Width = 872
    Height = 5
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
  end
  object Panel5: TPanel
    Left = 5
    Top = 5
    Width = 862
    Height = 501
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 4
    object ToolBar1: TToolBar
      Left = 0
      Top = 0
      Width = 862
      Height = 67
      ButtonHeight = 52
      ButtonWidth = 65
      Caption = 'ToolBar1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Images = DMImagens.ImagemBotaoCadastros32
      ParentFont = False
      ShowCaptions = True
      TabOrder = 0
      Wrapable = False
      object btnIncluir: TToolButton
        Left = 0
        Top = 0
        Action = acIncluir
        ParentShowHint = False
        ShowHint = True
      end
      object btnEditar: TToolButton
        Left = 65
        Top = 0
        Action = acEditar
      end
      object btnGravar: TToolButton
        Left = 130
        Top = 0
        Action = acGravar
      end
      object btnCancelar: TToolButton
        Left = 195
        Top = 0
        Action = acCancelar
      end
      object btnExcluir: TToolButton
        Left = 260
        Top = 0
        Action = acExcluir
      end
      object ToolButton2: TToolButton
        Left = 325
        Top = 0
        Width = 6
        Caption = 'ToolButton2'
        ImageIndex = 30
        Style = tbsSeparator
      end
      object ToolButton9: TToolButton
        Left = 331
        Top = 0
        Action = acRelatorio
      end
      object ToolButton7: TToolButton
        Left = 396
        Top = 0
        Width = 281
        Caption = 'ToolButton7'
        ImageIndex = 7
        Style = tbsSeparator
      end
      object ToolButton1: TToolButton
        Left = 677
        Top = 0
        Action = acSair
      end
    end
    object pagDados: TcxPageControl
      Left = 0
      Top = 67
      Width = 862
      Height = 434
      Align = alClient
      TabOrder = 1
      Properties.ActivePage = tabGrid
      Properties.CustomButtons.Buttons = <>
      ClientRectBottom = 430
      ClientRectLeft = 4
      ClientRectRight = 858
      ClientRectTop = 24
      object tabGrid: TcxTabSheet
        Caption = 'tabGrid'
        ImageIndex = 0
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 854
          Height = 406
          Align = alClient
          TabOrder = 0
          object cxGrid1: TcxGrid
            Left = 1
            Top = 1
            Width = 852
            Height = 404
            Align = alClient
            BorderStyle = cxcbsNone
            TabOrder = 0
            object cxGrid1DBTableView1: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              FindPanel.DisplayMode = fpdmAlways
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Kind = skCount
                  FieldName = 'CD_BAIRRO'
                  Column = cxGrid1DBTableView1CD_BAIRRO
                end>
              DataController.Summary.SummaryGroups = <>
              OptionsSelection.CellSelect = False
              OptionsView.ColumnAutoWidth = True
              OptionsView.Footer = True
              OptionsView.GroupByBox = False
              Styles.Header = DMImagens.Grid_Titulo
              object cxGrid1DBTableView1CD_BAIRRO: TcxGridDBColumn
                Caption = ' C'#243'digo'
                DataBinding.FieldName = 'CD_BAIRRO'
              end
              object cxGrid1DBTableView1DS_BAIRRO: TcxGridDBColumn
                Caption = ' Descri'#231#227'o'
                DataBinding.FieldName = 'DS_BAIRRO'
              end
            end
            object cxGrid1Level1: TcxGridLevel
              GridView = cxGrid1DBTableView1
            end
          end
        end
      end
      object tabDados: TcxTabSheet
        Caption = 'tabDados'
        ImageIndex = 1
      end
    end
  end
  object ACBotoes: TActionManager
    Left = 12
    Top = 244
    StyleName = 'Platform Default'
    object acIncluir: TAction
      Caption = 'Incluir F2'
      HelpKeyword = 'Incluir F2'
      Hint = 'Inclui um novo registro'
      ImageIndex = 2
      ShortCut = 113
    end
    object acEditar: TAction
      Caption = 'Editar F3'
      HelpKeyword = 'Editar'
      Hint = 'Altera o registro selecionado'
      ImageIndex = 7
      ShortCut = 114
    end
    object acGravar: TAction
      Caption = 'Gravar F4'
      HelpKeyword = 'Gravar'
      Hint = 'Grava as informa'#231#245'es incluidas/alteradas'
      ImageIndex = 0
      ShortCut = 115
    end
    object acCancelar: TAction
      Caption = 'Cancelar F5'
      HelpKeyword = 'Cancelar'
      Hint = 'Cancelar o registro selecionado'
      ImageIndex = 6
      ShortCut = 116
    end
    object acExcluir: TAction
      Caption = 'Excluir F6'
      HelpKeyword = 'Excluir'
      Hint = 'Excluir o registro selecionado'
      ImageIndex = 3
      ShortCut = 117
    end
    object acRelatorio: TAction
      Caption = 'Relat'#243'rio F7'
      HelpKeyword = 'Imprimir'
      Hint = 'Imprimir relat'#243'rio'
      ImageIndex = 13
      ShortCut = 118
    end
    object acSair: TAction
      Caption = 'Sair F12'
      HelpKeyword = 'Fechar a tela'
      Hint = 'Fechar a tela'
      ImageIndex = 29
      ShortCut = 123
    end
  end
  object qryGrid: TFDQuery
    SQL.Strings = (
      '')
    Left = 13
    Top = 198
  end
  object DSGrid: TDataSource
    DataSet = qryGrid
    Left = 49
    Top = 198
  end
  object qryDados: TFDQuery
    SQL.Strings = (
      '')
    Left = 97
    Top = 198
  end
  object DSDados: TDataSource
    DataSet = qryDados
    Left = 137
    Top = 198
  end
  object qryTemp: TFDQuery
    Connection = DMConexao.ConexaoFB
    SQL.Strings = (
      '')
    Left = 182
    Top = 200
  end
end

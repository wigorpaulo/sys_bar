object DMConexao: TDMConexao
  OldCreateOrder = False
  Height = 321
  Width = 494
  object ConexaoFB: TFDConnection
    Params.Strings = (
      'Database=D:\BASE_DADOS\SISTEMA.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'Port=3050'
      'DriverID=FB')
    FetchOptions.AssignedValues = [evRowsetSize]
    FetchOptions.RowsetSize = 1000
    ResourceOptions.AssignedValues = [rvAutoReconnect]
    ResourceOptions.AutoReconnect = True
    ConnectedStoredUsage = []
    Connected = True
    LoginPrompt = False
    Transaction = FDTransactionFB
    Left = 46
    Top = 18
  end
  object FDPhysIBDriverLinkFB: TFDPhysIBDriverLink
    Left = 48
    Top = 80
  end
  object FDGUIxWaitCursorFB: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 46
    Top = 141
  end
  object FDTransactionFB: TFDTransaction
    Connection = ConexaoFB
    Left = 46
    Top = 202
  end
end

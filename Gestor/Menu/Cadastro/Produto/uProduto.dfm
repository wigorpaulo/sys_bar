object fProduto: TfProduto
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Cadastro de produto'
  ClientHeight = 361
  ClientWidth = 684
  Color = clBtnFace
  Constraints.MaxHeight = 400
  Constraints.MaxWidth = 700
  Constraints.MinHeight = 400
  Constraints.MinWidth = 700
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIForm
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pagDados: TcxPageControl
    Left = 0
    Top = 0
    Width = 684
    Height = 361
    Align = alClient
    TabOrder = 0
    Properties.ActivePage = tabGrid
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 359
    ClientRectLeft = 2
    ClientRectRight = 682
    ClientRectTop = 28
    object tabGrid: TcxTabSheet
      Caption = 'tabGrid'
      ImageIndex = 0
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 680
        Height = 331
        Align = alClient
        TabOrder = 0
        object cxGrid1: TcxGrid
          Left = 1
          Top = 1
          Width = 678
          Height = 329
          Align = alClient
          BorderStyle = cxcbsNone
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            FindPanel.DisplayMode = fpdmAlways
            FindPanel.ShowClearButton = False
            FindPanel.ShowCloseButton = False
            FindPanel.ShowFindButton = False
            DataController.DataSource = DSGrid
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Kind = skCount
                FieldName = 'CD_FILIAL'
                Column = cxGrid1DBTableView1CD_FILIAL
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsSelection.CellSelect = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            object cxGrid1DBTableView1CD_FILIAL: TcxGridDBColumn
              Caption = ' C'#243'digo'
              DataBinding.FieldName = 'CD_FILIAL'
            end
            object cxGrid1DBTableView1NM_FILIAL: TcxGridDBColumn
              Caption = ' Descri'#231#227'o'
              DataBinding.FieldName = 'NM_FILIAL'
            end
            object cxGrid1DBTableView1NR_CGC: TcxGridDBColumn
              Caption = ' CGC'
              DataBinding.FieldName = 'NR_CGC'
            end
            object cxGrid1DBTableView1NR_INSCRICAO: TcxGridDBColumn
              Caption = ' Inscri'#231#227'o'
              DataBinding.FieldName = 'NR_INSCRICAO'
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
    end
    object tabDados: TcxTabSheet
      Caption = 'tabDados'
      ImageIndex = 1
      object cxTabControl2: TcxTabControl
        Left = 0
        Top = 0
        Width = 680
        Height = 331
        Align = alClient
        TabOrder = 0
        Properties.CustomButtons.Buttons = <>
        ClientRectBottom = 329
        ClientRectLeft = 2
        ClientRectRight = 678
        ClientRectTop = 2
      end
    end
  end
  object qryGrid: TFDQuery
    SQL.Strings = (
      'Select *'
      'from filial'
      'order by cd_filial')
    Left = 333
    Top = 134
    object qryGridCD_EMPRESA: TIntegerField
      FieldName = 'CD_EMPRESA'
      Origin = 'CD_EMPRESA'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryGridCD_FILIAL: TIntegerField
      FieldName = 'CD_FILIAL'
      Origin = 'CD_FILIAL'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryGridNM_FILIAL: TStringField
      FieldName = 'NM_FILIAL'
      Origin = 'NM_FILIAL'
      Size = 30
    end
    object qryGridDS_ENDERECO: TStringField
      FieldName = 'DS_ENDERECO'
      Origin = 'DS_ENDERECO'
      Size = 35
    end
    object qryGridDS_BAIRRO: TStringField
      FieldName = 'DS_BAIRRO'
      Origin = 'DS_BAIRRO'
      Size = 15
    end
    object qryGridCD_CIDADE: TIntegerField
      FieldName = 'CD_CIDADE'
      Origin = 'CD_CIDADE'
      Required = True
    end
    object qryGridNR_CEP: TStringField
      FieldName = 'NR_CEP'
      Origin = 'NR_CEP'
      Size = 9
    end
    object qryGridNR_CGC: TFloatField
      FieldName = 'NR_CGC'
      Origin = 'NR_CGC'
    end
    object qryGridNR_INSCRICAO: TStringField
      FieldName = 'NR_INSCRICAO'
      Origin = 'NR_INSCRICAO'
    end
    object qryGridNR_DDD: TStringField
      FieldName = 'NR_DDD'
      Origin = 'NR_DDD'
      Size = 4
    end
    object qryGridNR_TELEFONE: TStringField
      FieldName = 'NR_TELEFONE'
      Origin = 'NR_TELEFONE'
      Size = 10
    end
    object qryGridFG_VAREJOATACADO: TStringField
      FieldName = 'FG_VAREJOATACADO'
      Origin = 'FG_VAREJOATACADO'
      FixedChar = True
      Size = 1
    end
    object qryGridNM_GERENTE: TStringField
      FieldName = 'NM_GERENTE'
      Origin = 'NM_GERENTE'
    end
    object qryGridFG_ATUALESTOQUE: TStringField
      FieldName = 'FG_ATUALESTOQUE'
      Origin = 'FG_ATUALESTOQUE'
      Size = 1
    end
    object qryGridDT_INCALT: TSQLTimeStampField
      FieldName = 'DT_INCALT'
      Origin = 'DT_INCALT'
    end
    object qryGridCD_USRINCALT: TStringField
      FieldName = 'CD_USRINCALT'
      Origin = 'CD_USRINCALT'
      Size = 10
    end
  end
  object DSGrid: TDataSource
    Left = 377
    Top = 134
  end
  object qryDados: TFDQuery
    SQL.Strings = (
      'select *'
      'from filial'
      'where cd_empresa = :cd_empresa and'
      '      cd_filial  = :cd_filial')
    Left = 625
    Top = 150
    ParamData = <
      item
        Name = 'CD_EMPRESA'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'CD_FILIAL'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object qryDadosCD_EMPRESA: TIntegerField
      FieldName = 'CD_EMPRESA'
      Origin = 'CD_EMPRESA'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryDadosCD_FILIAL: TIntegerField
      FieldName = 'CD_FILIAL'
      Origin = 'CD_FILIAL'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryDadosNM_FILIAL: TStringField
      FieldName = 'NM_FILIAL'
      Origin = 'NM_FILIAL'
      Size = 30
    end
    object qryDadosDS_ENDERECO: TStringField
      FieldName = 'DS_ENDERECO'
      Origin = 'DS_ENDERECO'
      Size = 35
    end
    object qryDadosDS_BAIRRO: TStringField
      FieldName = 'DS_BAIRRO'
      Origin = 'DS_BAIRRO'
      Size = 15
    end
    object qryDadosCD_CIDADE: TIntegerField
      FieldName = 'CD_CIDADE'
      Origin = 'CD_CIDADE'
      Required = True
    end
    object qryDadosNR_CEP: TStringField
      FieldName = 'NR_CEP'
      Origin = 'NR_CEP'
      Size = 9
    end
    object qryDadosNR_CGC: TFloatField
      FieldName = 'NR_CGC'
      Origin = 'NR_CGC'
    end
    object qryDadosNR_INSCRICAO: TStringField
      FieldName = 'NR_INSCRICAO'
      Origin = 'NR_INSCRICAO'
    end
    object qryDadosNR_DDD: TStringField
      FieldName = 'NR_DDD'
      Origin = 'NR_DDD'
      Size = 4
    end
    object qryDadosNR_TELEFONE: TStringField
      FieldName = 'NR_TELEFONE'
      Origin = 'NR_TELEFONE'
      Size = 10
    end
    object qryDadosFG_VAREJOATACADO: TStringField
      FieldName = 'FG_VAREJOATACADO'
      Origin = 'FG_VAREJOATACADO'
      FixedChar = True
      Size = 1
    end
    object qryDadosNM_GERENTE: TStringField
      FieldName = 'NM_GERENTE'
      Origin = 'NM_GERENTE'
    end
    object qryDadosFG_ATUALESTOQUE: TStringField
      FieldName = 'FG_ATUALESTOQUE'
      Origin = 'FG_ATUALESTOQUE'
      Size = 1
    end
    object qryDadosDT_INCALT: TSQLTimeStampField
      FieldName = 'DT_INCALT'
      Origin = 'DT_INCALT'
    end
    object qryDadosCD_USRINCALT: TStringField
      FieldName = 'CD_USRINCALT'
      Origin = 'CD_USRINCALT'
      Size = 10
    end
  end
  object DSDados: TDataSource
    DataSet = qryDados
    Left = 625
    Top = 206
  end
  object ACBotoes: TActionManager
    Left = 188
    Top = 178
    StyleName = 'Platform Default'
    object acIncluir: TAction
      Caption = 'Incluir F2'
      HelpKeyword = 'Incluir F2'
      Hint = 'Inclui um novo registro'
      ImageIndex = 2
      ShortCut = 113
    end
    object acEditar: TAction
      Caption = 'Editar F3'
      HelpKeyword = 'Editar'
      Hint = 'Altera o registro selecionado'
      ImageIndex = 7
      ShortCut = 114
    end
    object acGravar: TAction
      Caption = 'Gravar F4'
      HelpKeyword = 'Gravar'
      Hint = 'Grava as informa'#231#245'es incluidas/alteradas'
      ImageIndex = 0
      ShortCut = 115
    end
    object acCancelar: TAction
      Caption = 'Cancelar F5'
      HelpKeyword = 'Cancelar'
      Hint = 'Cancelar o registro selecionado'
      ImageIndex = 6
      ShortCut = 116
    end
    object acExcluir: TAction
      Caption = 'Excluir F6'
      HelpKeyword = 'Excluir'
      Hint = 'Excluir o registro selecionado'
      ImageIndex = 3
      ShortCut = 117
    end
    object acRelatorio: TAction
      Caption = 'Relat'#243'rio F7'
      HelpKeyword = 'Imprimir'
      Hint = 'Imprimir relat'#243'rio'
      ImageIndex = 13
      ShortCut = 118
    end
    object acSair: TAction
      Caption = 'Sair F12'
      HelpKeyword = 'Fechar a tela'
      Hint = 'Fechar a tela'
      ImageIndex = 29
      ShortCut = 123
      OnExecute = acSairExecute
    end
  end
end

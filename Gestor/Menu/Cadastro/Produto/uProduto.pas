unit uProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.ToolWin, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,  FileCtrl,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, dxBarBuiltInMenu, cxContainer, cxEdit,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB,
  cxDBData, cxCheckBox, cxDBEdit, cxDBLookupComboBox, Vcl.StdCtrls, cxPC,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxTextEdit, cxMaskEdit, Vcl.ExtCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Actions, Vcl.ActnList,
  Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan, Vcl.Menus, cxButtons, Vcl.Buttons;


type
  TfProduto = class(TForm)
    pagDados: TcxPageControl;
    tabGrid: TcxTabSheet;
    Panel8: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    tabDados: TcxTabSheet;
    cxTabControl2: TcxTabControl;
    qryGrid: TFDQuery;
    DSGrid: TDataSource;
    qryDados: TFDQuery;
    DSDados: TDataSource;
    ACBotoes: TActionManager;
    acIncluir: TAction;
    acEditar: TAction;
    acGravar: TAction;
    acCancelar: TAction;
    acExcluir: TAction;
    acRelatorio: TAction;
    acSair: TAction;
    qryDadosCD_EMPRESA: TIntegerField;
    qryDadosCD_FILIAL: TIntegerField;
    qryDadosNM_FILIAL: TStringField;
    qryDadosDS_ENDERECO: TStringField;
    qryDadosDS_BAIRRO: TStringField;
    qryDadosCD_CIDADE: TIntegerField;
    qryDadosNR_CEP: TStringField;
    qryDadosNR_CGC: TFloatField;
    qryDadosNR_INSCRICAO: TStringField;
    qryDadosNR_DDD: TStringField;
    qryDadosNR_TELEFONE: TStringField;
    qryDadosFG_VAREJOATACADO: TStringField;
    qryDadosNM_GERENTE: TStringField;
    qryDadosFG_ATUALESTOQUE: TStringField;
    qryDadosDT_INCALT: TSQLTimeStampField;
    qryDadosCD_USRINCALT: TStringField;
    qryGridCD_EMPRESA: TIntegerField;
    qryGridCD_FILIAL: TIntegerField;
    qryGridNM_FILIAL: TStringField;
    qryGridDS_ENDERECO: TStringField;
    qryGridDS_BAIRRO: TStringField;
    qryGridCD_CIDADE: TIntegerField;
    qryGridNR_CEP: TStringField;
    qryGridNR_CGC: TFloatField;
    qryGridNR_INSCRICAO: TStringField;
    qryGridNR_DDD: TStringField;
    qryGridNR_TELEFONE: TStringField;
    qryGridFG_VAREJOATACADO: TStringField;
    qryGridNM_GERENTE: TStringField;
    qryGridFG_ATUALESTOQUE: TStringField;
    qryGridDT_INCALT: TSQLTimeStampField;
    qryGridCD_USRINCALT: TStringField;
    cxGrid1DBTableView1CD_FILIAL: TcxGridDBColumn;
    cxGrid1DBTableView1NM_FILIAL: TcxGridDBColumn;
    cxGrid1DBTableView1NR_CGC: TcxGridDBColumn;
    cxGrid1DBTableView1NR_INSCRICAO: TcxGridDBColumn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure AbreQuery;
    procedure ToolButton1Click(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure acSairExecute(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    bEditaRel : Boolean;
    function ValidarCampos : Boolean;
    procedure PesquisaCidade;
  public
    { Public declarations }
  end;

var
  fProduto: TfProduto;

implementation

{$R *.dfm}

uses uPrincipal;

procedure TfProduto.AbreQuery;
begin
  qryGrid.Close;
  qryGrid.Open();
end;

procedure TfProduto.acSairExecute(Sender: TObject);
begin
  Close;
end;

procedure TfProduto.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if qryDados.State in [dsInsert, dsEdit] then
    qryDados.Cancel;

  fProduto := nil;
  Action := CaFree;
end;

procedure TfProduto.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if      (Key = #27) and (DSDados.DataSet.State = dsBrowse) then
          Close
  else if Key = #13 then
          Perform(wm_nextdlgctl, 0, 0);
end;

procedure TfProduto.FormShow(Sender: TObject);
begin
//  pagDados.ActivePage := tabGrid;
  pagDados.HideTabs   := True;
//
//  AbreQuery;
end;

procedure TfProduto.PesquisaCidade;
begin
end;

procedure TfProduto.ToolButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfProduto.btnIncluirClick(Sender: TObject);
begin
  acIncluir.Execute;
end;

procedure TfProduto.btnCancelarClick(Sender: TObject);
begin
  acCancelar.Execute;
end;

procedure TfProduto.ToolButton6Click(Sender: TObject);
begin
  acExcluir.Execute;
end;

function TfProduto.ValidarCampos: Boolean;
begin

  Result := true;
end;

end.

object fPais: TfPais
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = 'Pa'#237's'
  ClientHeight = 339
  ClientWidth = 658
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIForm
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 653
    Top = 5
    Width = 5
    Height = 329
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 628
    ExplicitTop = 8
    ExplicitHeight = 313
  end
  object Panel2: TPanel
    Left = 0
    Top = 5
    Width = 5
    Height = 329
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 313
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 658
    Height = 5
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 629
  end
  object Panel4: TPanel
    Left = 0
    Top = 334
    Width = 658
    Height = 5
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitTop = 318
    ExplicitWidth = 629
  end
  object Panel5: TPanel
    Left = 5
    Top = 5
    Width = 648
    Height = 329
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 4
    ExplicitWidth = 619
    ExplicitHeight = 313
    object pagPrincipal: TcxPageControl
      Left = 0
      Top = 67
      Width = 648
      Height = 262
      Align = alClient
      TabOrder = 0
      Properties.ActivePage = tabPrincipal_Dados
      Properties.CustomButtons.Buttons = <>
      ExplicitLeft = 6
      ExplicitTop = 73
      ExplicitWidth = 632
      ExplicitHeight = 246
      ClientRectBottom = 258
      ClientRectLeft = 4
      ClientRectRight = 644
      ClientRectTop = 24
      object tabPrincipal_Grid: TcxTabSheet
        Caption = 'tabPrincipal_Grid'
        ImageIndex = 0
        ExplicitWidth = 611
        ExplicitHeight = 218
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 640
          Height = 234
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 611
          ExplicitHeight = 218
          object cxGrid: TcxGrid
            Left = 1
            Top = 1
            Width = 638
            Height = 232
            Align = alClient
            BorderStyle = cxcbsNone
            TabOrder = 0
            ExplicitWidth = 609
            ExplicitHeight = 216
            object cxGridDBTableView1: TcxGridDBTableView
              OnDblClick = cxGridDBTableView1DblClick
              Navigator.Buttons.CustomButtons = <>
              FindPanel.DisplayMode = fpdmAlways
              DataController.DataSource = DSGrid
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Kind = skCount
                  FieldName = 'PAS_CODIGO'
                  Column = cxGridDBTableView1PAS_CODIGO
                end>
              DataController.Summary.SummaryGroups = <>
              OptionsSelection.CellSelect = False
              OptionsView.ColumnAutoWidth = True
              OptionsView.Footer = True
              OptionsView.GroupByBox = False
              Styles.Header = DMImagens.Grid_Titulo
              object cxGridDBTableView1PAS_CODIGO: TcxGridDBColumn
                Caption = 'C'#243'digo'
                DataBinding.FieldName = 'PAS_CODIGO'
                Width = 58
              end
              object cxGridDBTableView1PAS_DESCRICAO: TcxGridDBColumn
                Caption = 'Descri'#231#227'o'
                DataBinding.FieldName = 'PAS_DESCRICAO'
                Width = 462
              end
              object cxGridDBTableView1PAS_SIGLA: TcxGridDBColumn
                Caption = 'Sigla'
                DataBinding.FieldName = 'PAS_SIGLA'
                Width = 102
              end
            end
            object cxGridLevel1: TcxGridLevel
              GridView = cxGridDBTableView1
            end
          end
        end
      end
      object tabPrincipal_Dados: TcxTabSheet
        Caption = 'tabPrincipal_Dados'
        ImageIndex = 1
        ExplicitWidth = 621
        ExplicitHeight = 218
        object pagDados: TcxPageControl
          Left = 0
          Top = 0
          Width = 640
          Height = 234
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          Properties.ActivePage = TabDados
          Properties.CustomButtons.Buttons = <>
          ExplicitLeft = -4
          ExplicitTop = 51
          ExplicitWidth = 629
          ExplicitHeight = 193
          ClientRectBottom = 230
          ClientRectLeft = 4
          ClientRectRight = 636
          ClientRectTop = 24
          object TabDados: TcxTabSheet
            Caption = 'Dados'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ImageIndex = 0
            ParentFont = False
            ExplicitLeft = 5
            ExplicitTop = 25
            ExplicitWidth = 616
            ExplicitHeight = 190
            object Label5: TLabel
              Left = 7
              Top = 10
              Width = 38
              Height = 13
              Caption = 'C'#243'digo'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label6: TLabel
              Left = 97
              Top = 10
              Width = 55
              Height = 13
              Caption = 'Descri'#231#227'o'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label1: TLabel
              Left = 549
              Top = 10
              Width = 27
              Height = 13
              Caption = 'Sigla'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object cxDBTextEdit1: TcxDBTextEdit
              Left = 7
              Top = 24
              DataBinding.DataField = 'PAS_CODIGO'
              DataBinding.DataSource = DSDados
              Enabled = False
              ParentFont = False
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = []
              Style.IsFontAssigned = True
              TabOrder = 0
              Width = 83
            end
            object edtDescricao: TcxDBTextEdit
              Left = 96
              Top = 24
              DataBinding.DataField = 'PAS_DESCRICAO'
              DataBinding.DataSource = DSDados
              ParentFont = False
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = []
              Style.IsFontAssigned = True
              TabOrder = 1
              Width = 447
            end
            object cxDBTextEdit2: TcxDBTextEdit
              Left = 549
              Top = 24
              DataBinding.DataField = 'PAS_SIGLA'
              DataBinding.DataSource = DSDados
              ParentFont = False
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = []
              Style.IsFontAssigned = True
              TabOrder = 2
              Width = 76
            end
          end
        end
      end
    end
    object ToolBar1: TToolBar
      Left = 0
      Top = 0
      Width = 648
      Height = 67
      ButtonHeight = 52
      ButtonWidth = 65
      Caption = 'ToolBar1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Images = DMImagens.ImagemBotaoCadastros32
      ParentFont = False
      ShowCaptions = True
      TabOrder = 1
      Wrapable = False
      ExplicitTop = 8
      ExplicitWidth = 619
      object btnIncluir: TToolButton
        Left = 0
        Top = 0
        Action = acIncluir
        ParentShowHint = False
        ShowHint = True
      end
      object btnEditar: TToolButton
        Left = 65
        Top = 0
        Action = acEditar
      end
      object btnGravar: TToolButton
        Left = 130
        Top = 0
        Action = acGravar
      end
      object btnCancelar: TToolButton
        Left = 195
        Top = 0
        Action = acCancelar
      end
      object btnExcluir: TToolButton
        Left = 260
        Top = 0
        Action = acExcluir
      end
      object ToolButton2: TToolButton
        Left = 325
        Top = 0
        Width = 6
        Caption = 'ToolButton2'
        ImageIndex = 30
        Style = tbsSeparator
      end
      object ToolButton9: TToolButton
        Left = 331
        Top = 0
        Action = acRelatorio
      end
      object ToolButton7: TToolButton
        Left = 396
        Top = 0
        Width = 187
        Caption = 'ToolButton7'
        ImageIndex = 7
        Style = tbsSeparator
      end
      object ToolButton1: TToolButton
        Left = 583
        Top = 0
        Action = acSair
      end
    end
  end
  object ACBotoes: TActionManager
    Images = DMImagens.ImagemBotaoCadastros32
    Left = 15
    Top = 227
    StyleName = 'Platform Default'
    object acIncluir: TAction
      Caption = 'Incluir F2'
      HelpKeyword = 'Incluir F2'
      Hint = 'Inclui um novo registro'
      ImageIndex = 2
      ShortCut = 113
      OnExecute = acIncluirExecute
    end
    object acEditar: TAction
      Caption = 'Editar F3'
      HelpKeyword = 'Editar'
      Hint = 'Altera o registro selecionado'
      ImageIndex = 7
      ShortCut = 114
      OnExecute = acEditarExecute
    end
    object acGravar: TAction
      Caption = 'Gravar F4'
      HelpKeyword = 'Gravar'
      Hint = 'Grava as informa'#231#245'es incluidas/alteradas'
      ImageIndex = 0
      ShortCut = 115
      OnExecute = acGravarExecute
    end
    object acCancelar: TAction
      Caption = 'Cancelar F5'
      HelpKeyword = 'Cancelar'
      Hint = 'Cancelar o registro selecionado'
      ImageIndex = 6
      ShortCut = 116
      OnExecute = acCancelarExecute
    end
    object acExcluir: TAction
      Caption = 'Excluir F6'
      HelpKeyword = 'Excluir'
      Hint = 'Excluir o registro selecionado'
      ImageIndex = 3
      ShortCut = 117
      OnExecute = acExcluirExecute
    end
    object acRelatorio: TAction
      Caption = 'Relat'#243'rio F7'
      HelpKeyword = 'Imprimir'
      Hint = 'Imprimir relat'#243'rio'
      ImageIndex = 13
      ShortCut = 118
    end
    object acSair: TAction
      Caption = 'Sair F12'
      HelpKeyword = 'Fechar a tela'
      Hint = 'Fechar a tela'
      ImageIndex = 29
      ShortCut = 123
      OnExecute = acSairExecute
    end
  end
  object qryGrid: TFDQuery
    Connection = DMConexao.ConexaoFB
    SQL.Strings = (
      'Select *'
      'From GER_PAIS'
      'Order By 1')
    Left = 13
    Top = 182
    object qryGridPAS_CODIGO: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'PAS_CODIGO'
      Origin = 'PAS_CODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryGridPAS_DESCRICAO: TStringField
      FieldName = 'PAS_DESCRICAO'
      Origin = 'PAS_DESCRICAO'
      Required = True
      Size = 255
    end
    object qryGridPAS_SIGLA: TStringField
      FieldName = 'PAS_SIGLA'
      Origin = 'PAS_SIGLA'
      Required = True
      Size = 10
    end
  end
  object DSGrid: TDataSource
    DataSet = qryGrid
    Left = 49
    Top = 182
  end
  object qryDados: TFDQuery
    Connection = DMConexao.ConexaoFB
    SQL.Strings = (
      'Select PAS_CODIGO,'
      '       PAS_DESCRICAO,'
      '       PAS_SIGLA'
      'From GER_PAIS'
      'Where PAS_CODIGO = :pPAS_CODIGO')
    Left = 86
    Top = 184
    ParamData = <
      item
        Name = 'PPAS_CODIGO'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object qryDadosPAS_CODIGO: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'PAS_CODIGO'
      Origin = 'PAS_CODIGO'
      ProviderFlags = []
    end
    object qryDadosPAS_DESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'PAS_DESCRICAO'
      Origin = 'PAS_DESCRICAO'
      Required = True
      Size = 255
    end
    object qryDadosPAS_SIGLA: TStringField
      DisplayLabel = 'Sigla'
      FieldName = 'PAS_SIGLA'
      Origin = 'PAS_SIGLA'
      Required = True
      Size = 10
    end
  end
  object DSDados: TDataSource
    DataSet = qryDados
    Left = 124
    Top = 184
  end
  object qrySelect: TFDQuery
    Connection = DMConexao.ConexaoFB
    SQL.Strings = (
      '')
    Left = 190
    Top = 176
  end
end

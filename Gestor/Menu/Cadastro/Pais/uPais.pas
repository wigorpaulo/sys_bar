unit uPais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.ToolWin, uDMRelatorio, uDMImagens, System.Actions, Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxPC, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  uDMConexao, cxContainer, Vcl.StdCtrls, cxTextEdit, cxDBEdit, System.ImageList, Vcl.ImgList, uUtil;

type
  TfPais = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    ACBotoes: TActionManager;
    acIncluir: TAction;
    acEditar: TAction;
    acGravar: TAction;
    acCancelar: TAction;
    acExcluir: TAction;
    acRelatorio: TAction;
    acSair: TAction;
    pagPrincipal: TcxPageControl;
    tabPrincipal_Grid: TcxTabSheet;
    Panel8: TPanel;
    cxGrid: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    tabPrincipal_Dados: TcxTabSheet;
    qryGrid: TFDQuery;
    DSGrid: TDataSource;
    qryDados: TFDQuery;
    DSDados: TDataSource;
    qryGridPAS_CODIGO: TIntegerField;
    qryGridPAS_DESCRICAO: TStringField;
    qryGridPAS_SIGLA: TStringField;
    cxGridDBTableView1PAS_CODIGO: TcxGridDBColumn;
    cxGridDBTableView1PAS_DESCRICAO: TcxGridDBColumn;
    cxGridDBTableView1PAS_SIGLA: TcxGridDBColumn;
    qryDadosPAS_CODIGO: TIntegerField;
    qryDadosPAS_DESCRICAO: TStringField;
    qryDadosPAS_SIGLA: TStringField;
    ToolBar1: TToolBar;
    btnIncluir: TToolButton;
    btnEditar: TToolButton;
    btnGravar: TToolButton;
    btnCancelar: TToolButton;
    btnExcluir: TToolButton;
    ToolButton2: TToolButton;
    ToolButton9: TToolButton;
    ToolButton7: TToolButton;
    ToolButton1: TToolButton;
    qrySelect: TFDQuery;
    pagDados: TcxPageControl;
    TabDados: TcxTabSheet;
    Label5: TLabel;
    cxDBTextEdit1: TcxDBTextEdit;
    edtDescricao: TcxDBTextEdit;
    Label6: TLabel;
    Label1: TLabel;
    cxDBTextEdit2: TcxDBTextEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure acSairExecute(Sender: TObject);
    procedure acIncluirExecute(Sender: TObject);
    procedure acEditarExecute(Sender: TObject);
    procedure acGravarExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure acCancelarExecute(Sender: TObject);
    procedure acExcluirExecute(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
    procedure AbreQuery;
    procedure GravarDados;
    function VerificaCampo: Boolean;

  public
    { Public declarations }
  end;

var
  fPais: TfPais;

implementation

{$R *.dfm}

procedure TfPais.AbreQuery;
begin
  qryGrid.Close;
  qryGrid.Open;
end;

procedure TfPais.acCancelarExecute(Sender: TObject);
begin
  if qryDados.State in [dsInsert, dsEdit] then
  begin
    qryDados.Cancel;
    pagPrincipal.ActivePage := tabPrincipal_Grid;
    HabilitaBotao(ACBotoes, DSGrid, DSDados);
  end;
end;

procedure TfPais.acEditarExecute(Sender: TObject);
begin
  if not qryGrid.IsEmpty then
  begin
    try
      AcaoBotao(qryDados, qryGrid, 'PAS_CODIGO', 2);

      pagPrincipal.ActivePage := tabPrincipal_Dados;
      SetaFoco(edtDescricao);
      HabilitaBotao(ACBotoes, DSGrid, DSDados);
    except
      On E:Exception do
      begin
        pagPrincipal.ActivePage := tabPrincipal_Grid;
        MensagemErro('Erro ao alterar o registro!'+sLineBreak+ 'Erro: '+E.Message);
      end;
    end;
  end;
end;

procedure TfPais.acExcluirExecute(Sender: TObject);
begin
  if MensagemConfirmacao('Confirma a exclus�o do registro selecionado ?') = IDYES then
    qryGrid.Delete;

  HabilitaBotao(ACBotoes, DSGrid, DSDados);
end;

procedure TfPais.acGravarExecute(Sender: TObject);
begin
  if not VerificaCampo then
    Exit;

  GravarDados;
end;

procedure TfPais.acIncluirExecute(Sender: TObject);
begin
  try
    AcaoBotao(qryDados, qryGrid, 'PAS_CODIGO', 1);

    pagPrincipal.ActivePage := tabPrincipal_Dados;

    SetaFoco(edtDescricao);
    HabilitaBotao(ACBotoes, DSGrid, DSDados);
  except on E: Exception do
    begin
      pagPrincipal.ActivePage := tabPrincipal_Grid;
      MensagemErro('Erro ao incluir um registro...' + sLineBreak + 'Erro: ' + E.Message);
    end;
  end;
end;

procedure TfPais.acSairExecute(Sender: TObject);
begin
  Close;
end;

procedure TfPais.cxGridDBTableView1DblClick(Sender: TObject);
begin
  acEditar.Execute;
end;

procedure TfPais.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  fPais := nil;
  Action := CaFree;
end;

procedure TfPais.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if      (Key = #27) and (DSDados.DataSet.State = dsBrowse) then
          Close
  else if Key = #13 then
          Perform(wm_nextdlgctl, 0, 0);
end;

procedure TfPais.FormShow(Sender: TObject);
begin
  fPais.Height := 368;
  fPais.Width  := 664;

  AbreQuery;

  pagPrincipal.ActivePageIndex := 0;
  pagPrincipal.HideTabs        := true;

  pagDados.ActivePageIndex := 0;

  HabilitaBotao(ACBotoes, DSGrid, DSDados);
end;

procedure TfPais.GravarDados;
begin
  if not (qryDados.State in [dsEdit, dsInsert]) then
    Exit;

  try
    DMConexao.ConexaoFB.StartTransaction;

    qryDados.Post;
    pagPrincipal.ActivePage := tabPrincipal_Grid;

    DMConexao.ConexaoFB.Commit;

    AbreQuery;

    SetaFoco(cxGrid);
    HabilitaBotao(ACBotoes, DSGrid, DSDados);
  except on E: Exception do
    begin
      DMConexao.ConexaoFB.Rollback;
      pagPrincipal.ActivePage := tabPrincipal_Grid;
      MensagemErro('Erro ao gravar registro no banco de dados...' + sLineBreak + 'Erro: ' + E.Message);
    end;
  end;
end;

function TfPais.VerificaCampo: Boolean;
begin
  SetaFoco(ToolBar1);
  Result := ValidaCampo(DSDados);
end;

end.

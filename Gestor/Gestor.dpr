program Gestor;

uses
  Vcl.Forms,
  uPrincipal in 'uPrincipal.pas' {fPrincipal},
  uProduto in 'Menu\Cadastro\Produto\uProduto.pas' {fProduto},
  uDMImagens in '..\Funcoes\uDMImagens.pas' {DMImagens: TDataModule},
  uDMRelatorioComum in '..\Funcoes\DMRelatorioComum\uDMRelatorioComum.pas' {DMRelatorioComum: TDataModule},
  uDMRelatorio in 'DMRelatorio\uDMRelatorio.pas' {DMRelatorio: TDataModule},
  uDMConexao in 'Conexao\uDMConexao.pas' {DMConexao: TDataModule},
  uUtil in '..\Funcoes\uUtil.pas',
  uPais in 'Menu\Cadastro\Pais\uPais.pas' {fPais},
  uCadGer in 'GER_\uCadGer.pas' {fCadGer};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDMConexao, DMConexao);
  Application.CreateForm(TDMImagens, DMImagens);
  Application.CreateForm(TDMRelatorioComum, DMRelatorioComum);
  Application.CreateForm(TDMRelatorio, DMRelatorio);
  Application.CreateForm(TfPrincipal, fPrincipal);
  Application.Run;
end.

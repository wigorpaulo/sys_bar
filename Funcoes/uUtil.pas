unit uUtil;

interface

uses
  Vcl.Forms, Winapi.Windows, System.Classes, System.SysUtils, Winapi.ActiveX,
  Vcl.PlatformDefaultStyleActnCtrls, System.Actions, Vcl.ActnList, Vcl.ActnMan,
  Data.DB, FireDAC.Comp.Client;


{ procedures e function }
{ INICIO }
procedure MensagemInformacao(pTexto : string);
procedure MensagemAtencao(pTexto : string);
procedure MensagemErro(pTexto : string);
function MensagemConfirmacao(pTexto : string) : Integer;
function MensagemAjuda(pTexto : string) : Integer;

function GetPedaco(pTexto : string; pDelimitador : Char; pPosicao : Integer) : string;

procedure HabilitaBotao(pAction : TActionManager; pDataSourceGrid, pDataSourceDados : TDataSource);

procedure ShowTabSheet(AComponent: TComponent);
function SetaFoco(AComponent: TComponent): Boolean;

procedure AcaoBotao(pQryDados, pQryGrid : TFDQuery; pNomeParametro : string; pAcao : Integer);

function ValidaCampo(pDataSourceDados : TDataSource): Boolean;


{ FIM }

implementation

uses
  Vcl.Controls, Vcl.ComCtrls;

procedure MensagemInformacao(pTexto : string);
begin
  Application.MessageBox(PChar(pTexto),'Informativo',MB_ICONINFORMATION);
end;

procedure MensagemAtencao(pTexto : string);
begin
  Application.MessageBox(PChar(pTexto),'Aten��o',MB_ICONWARNING);
end;

procedure MensagemErro(pTexto : string);
begin
  Application.MessageBox(PChar(pTexto),'Erro',MB_ICONERROR);
end;

function MensagemConfirmacao(pTexto : string) : Integer;
begin
  Result := Application.MessageBox(PChar(pTexto),'Confirma��o',MB_YESNO);
end;

function MensagemAjuda(pTexto : string) : Integer;
begin
  Result := Application.MessageBox(PChar(pTexto),'Ajuda',MB_HELP);
end;

function GetPedaco(pTexto : string; pDelimitador : Char; pPosicao : Integer) : string;
var
  pedaco : TStringList;
  vCopy, vAux : string;
  I : Integer;
begin
  if Trim(pTexto) = '' then
    Result := pTexto
  else
  begin
    try
      pedaco := TStringList.Create;

      vAux := '';
      for I := 1 to Length(pTexto) do
      begin
        vCopy := Copy(pTexto,I,1);
        if vCopy <> pDelimitador then
          vAux := vAux+vCopy
        else
        begin
          pedaco.Append(vAux);
          vAux := '';
        end;
      end;

      if Trim(vAux) <> '' then
      begin
        pedaco.Append(vAux);
        vAux := '';
      end;

      try
        Result := pedaco.Strings[pPosicao];
      except
        on E: Exception do begin
          Result := '';
        end;
      end;
    finally
      FreeAndNil(pedaco);
    end;
  end;
end;

procedure HabilitaBotao(pAction : TActionManager; pDataSourceGrid, pDataSourceDados : TDataSource);
begin
  if ((pDataSourceGrid.State in [dsBrowse, dsInactive]) and (pDataSourceGrid.DataSet.RecordCount = 0)) and
      (pDataSourceDados.State in [dsBrowse, dsInactive]) then  // em branco, sem registro
  begin
    pAction.Actions[0].Enabled := True;  // Incluir
    pAction.Actions[1].Enabled := False; // Editar
    pAction.Actions[2].Enabled := False; // Gravar
    pAction.Actions[3].Enabled := False; // Cancelar
    pAction.Actions[4].Enabled := False; // Excluir
    pAction.Actions[5].Enabled := False; // Relatorio
  end
  else if (pDataSourceGrid.DataSet.RecordCount > 0) and (pDataSourceDados.State in [dsBrowse, dsInactive]) then
  begin
    pAction.Actions[0].Enabled := True;  // Incluir
    pAction.Actions[1].Enabled := True;  // Editar
    pAction.Actions[2].Enabled := False; // Gravar
    pAction.Actions[3].Enabled := False; // Cancelar
    pAction.Actions[4].Enabled := True;  // Excluir
    pAction.Actions[5].Enabled := False; // Relatorio
  end
  else if pDataSourceDados.State in [dsInsert, dsEdit] then
  begin
    pAction.Actions[0].Enabled := False;  // Incluir
    pAction.Actions[1].Enabled := False;  // Editar
    pAction.Actions[2].Enabled := True;   // Gravar
    pAction.Actions[3].Enabled := True;   // Cancelar
    pAction.Actions[4].Enabled := False;  // Excluir
    pAction.Actions[5].Enabled := False;  // Relatorio
  end;
end;

procedure ShowTabSheet(AComponent: TComponent);
begin
  if AComponent is TForm then
    Exit;

  if (AComponent is TWinControl) then
  begin
    if Assigned(TWinControl(AComponent).Parent) then
    begin
      if (TWinControl(AComponent).Parent is TTabSheet) then
      begin
        TTabSheet(TWinControl(AComponent).Parent).PageControl.TabIndex := TTabSheet(TWinControl(AComponent).Parent).PageIndex;
        ShowTabSheet(TTabSheet(TWinControl(AComponent).Parent).PageControl);
      end
      else
        ShowTabSheet(TComponent(TWinControl(AComponent).Parent));
    end;
  end
  else if Assigned(AComponent.Owner) then
    ShowTabSheet(AComponent.Owner);
end;

function SetaFoco(AComponent: TComponent): Boolean;
begin
  Result := False;

  if (AComponent is TWinControl) then
  begin
    ShowTabSheet(AComponent);

    if TWinControl(AComponent).CanFocus then
    begin
      TWinControl(AComponent).SetFocus;
      Result := True;
    end;
  end;
end;

procedure AcaoBotao(pQryDados, pQryGrid : TFDQuery; pNomeParametro : string; pAcao : Integer);
begin
  case pAcao of
    1 : // Incluir
    begin
      pQryDados.Close;
      pQryDados.ParamByName('p'+pNomeParametro).AsInteger := -1;
      pQryDados.Open;

      pQryDados.Append;
    end;
    2 : // Editar
    begin
      pQryDados.Close;
      pQryDados.ParamByName('p'+pNomeParametro).AsInteger := pQryGrid.FieldByName(pNomeParametro).AsInteger;
      pQryDados.Open;

      pQryDados.Edit;
    end;
  end;
end;

function ValidaCampo(pDataSourceDados : TDataSource): Boolean;
var
  i : Integer;
begin
  Result := True;
  for i := 0 to pDataSourceDados.DataSet.FieldCount - 1 do
  begin
    if pDataSourceDados.DataSet.Fields[i].Required then
      if (pDataSourceDados.DataSet.Fields[i].IsNull) or (pDataSourceDados.DataSet.Fields[i].AsString = EmptyStr) then
         begin
           MensagemAtencao('� obrigat�rio o preenchimento do campo "'+pDataSourceDados.DataSet.Fields[i].DisplayLabel+'"!');

           Result := false;
           Break;
         end;
  end;
end;

end.
